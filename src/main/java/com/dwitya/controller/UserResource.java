package com.dwitya.controller;

import com.dwitya.exception.ResourceNotFoundException;
import com.dwitya.model.User;
import com.dwitya.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.ws.rs.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Path("/api/v1")
public class UserResource {

    @Autowired
    private UserRepository userRepository;

    @GET
    @Produces("application/json")
    @Path("/users")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GET
    @Produces("application/json")
    @Path("/users/{id}")
    public ResponseEntity<User> getUserById(@PathParam(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User Not Found :: " + userId));
        return ResponseEntity.ok().body(user);
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    @Path("/users")
    @PostMapping("/users")
    public User createUser(User user){
        return userRepository.save(user);
    }

//    @PUT
//    @Produces("application/json")
//    @Consumes("application/json")
//    @Path("/users/{id}")
//    public User updateUsers(@PathParam(value = "id") Long userId,
//         @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
//        User user = userRepository.findById(userId)
//                .orElseThrow(() -> new ResourceNotFoundException("User not found :: "));
//        user.setEmailId(userDetails.getEmailId());
//        user.setFirstName(userDetails.getFirstName());
//        user.setLastName(userDetails.getLastName());
//        return userRepository.save(user);
//    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    @Path("/users/{id}")
    public ResponseEntity<User> updateUsers(@PathParam(value = "id") Long userId,
        @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        try {
            User user = userRepository.findById(userId).
                    orElseThrow(() -> new ResourceNotFoundException("User not found :: "));
            user.setEmailId(userDetails.getEmailId());
            user.setFirstName(userDetails.getFirstName());
            user.setLastName(userDetails.getLastName());
            final User updatedUser = userRepository.save(user);
            return ResponseEntity.ok().body(updatedUser);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

//    @DELETE
//    @Path("/users/{id}")
//    public void deleteUser(@PathParam(value = "id") Long userId) throws ResourceNotFoundException {
//        User user = userRepository.findById(userId)
//                .orElseThrow(() -> new ResourceNotFoundException("User not found :: " + userId));
//        userRepository.delete(user);
//
//    }

    @DELETE
    @Path("/users/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Map<String, Boolean> deleteUser(@PathParam(value = "id") Long userId) throws ResourceNotFoundException {
        Map<String, Boolean> response = new HashMap<>();
        try {
            User user = userRepository.findById(userId).
                    orElseThrow(() -> new ResourceNotFoundException("User not found :: " + userId));
            userRepository.delete(user);
            response.put("delete", Boolean.TRUE);
            return response;
        } catch(Exception e) {
            e.printStackTrace();
            response.put(e.getMessage(), Boolean.FALSE);
            return response;
        }
    }
}
