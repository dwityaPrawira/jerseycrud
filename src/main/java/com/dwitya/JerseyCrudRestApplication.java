package com.dwitya;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JerseyCrudRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(JerseyCrudRestApplication.class, args);
    }
}
