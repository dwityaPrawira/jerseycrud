package com.dwitya.config;

import com.dwitya.controller.UserResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/jersey")
public class JerseyConfiguration extends ResourceConfig {
    public JerseyConfiguration() {
        register(UserResource.class);
    }
}